<?php get_header(); ?>

<?php
$vdh_options = get_option('vdh_theme_options');
$frontend_slider_category = isset($vdh_options['frontend_slider']) ?
        abs((int) $vdh_options['frontend_slider']) :
        '';
Vdh_Template_Functions::add_template('slider/carousel', array(
    'slides' => get_posts(array(
        'category' => $frontend_slider_category,
        'posts_per_page' => 99987,
    ))
))
?>


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-md-4 text-center">
            <img class="img-circle" src="http://placehold.it/140x140">
            <h2>Mobile-first</h2>
            <p>Tablets, phones, laptops. The new 3 promises to be mobile friendly from the start.</p>
            <p><a class="btn btn-default" href="#">View details »</a></p>
        </div>
        <div class="col-md-4 text-center">
            <img class="img-circle" src="http://placehold.it/140x140">
            <h2>One Fluid Grid</h2>
            <p>There is now just one percentage-based grid for Bootstrap 3. Customize for fixed widths.</p>
            <p><a class="btn btn-default" href="#">View details »</a></p>
        </div>
        <div class="col-md-4 text-center">
            <img class="img-circle" src="http://placehold.it/140x140">
            <h2>LESS is More</h2>
            <p>Improved support for mixins make the new Bootstrap 3 easier to customize.</p>
            <p><a class="btn btn-default" href="#">View details »</a></p>
        </div>
    </div><!-- /.row -->


    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

    <div class="featurette">
        <img class="featurette-image img-circle pull-right" src="http://placehold.it/512">
        <h2 class="featurette-heading">Responsive Design. <span class="text-muted">It'll blow your mind.</span></h2>
        <p class="lead">In simple terms, a responsive web design figures out what resolution of device it's being served on. Flexible grids then size correctly to fit the screen.</p>
    </div>

    <hr class="featurette-divider">

    <div class="featurette">
        <img class="featurette-image img-circle pull-left" src="http://placehold.it/512">
        <h2 class="featurette-heading">Smaller Footprint. <span class="text-muted">Lightweight.</span></h2>
        <p class="lead">The new Bootstrap 3 promises to be a smaller build. The separate Bootstrap base and responsive.css files have now been merged into one. There is no more fixed grid, only fluid.</p>
    </div>

    <hr class="featurette-divider">

    <div class="featurette">
        <img class="featurette-image img-circle pull-right" src="http://placehold.it/512">
        <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Flatness.</span></h2>
        <p class="lead">A big design trend for 2013 is "flat" design. Gone are the days of excessive gradients and shadows. Designers are producing cleaner flat designs, and Bootstrap 3 takes advantage of this minimalist trend.</p>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->




























    <?php if (false): //hide it for a while   ?>
        <div role="main">
            <div id="content" class="container">
                <div class="flash-messages"></div>
                <div class="toolbar"></div>
                <div class="wrapper">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="primary">
                                <section class="module">
                                    <div class="module-content">
                                        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
                                            <header>
                                                <div class="cat-hadding">
                                                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                                                </div>
                                                <p class="meta post-meta-entry"><?php Vdh_Template_Functions::vdh_entry_meta(); ?></p>
                                                <p class="meta post-meta-entry"><?php the_tags(); ?></p>

                                            </header>
                                            <section class="post_content">
                                                <?php the_post_thumbnail('wpbs-featured'); ?>
                                                <?php the_excerpt(); ?>
                                            </section>
                                        </article>
                                    </div>
                                </section>
                            </div>
                            <aside class="secondary">
                                <?php get_sidebar(); ?>
                            </aside>


                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if (function_exists('faster_pagination')): ?>
                        <?php faster_pagination(); ?>
                    <?php else: ?>
                        <?php if (get_option('posts_per_page ') < $wp_query->found_posts): ?>
                            <nav class="vdh-nav">
                                <span class="vdh-nav-previous"><?php previous_posts_link(); ?></span>
                                <span class="vdh-nav-next"><?php next_posts_link(); ?></span>
                            </nav>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>


            </div>
        </div>
    <?php endif; ?>
    <!-- end #content -->
    <?php get_footer(); ?>
