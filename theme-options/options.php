<?php

function vdh_options_init() {
    register_setting('vdh_options', 'vdh_theme_options', 'vdh_options_validate');
}

add_action('admin_init', 'vdh_options_init');

function vdh_options_validate($input) {
    $input['logo'] = esc_url_raw($input['logo']);
    $input['favicon'] = esc_url_raw($input['favicon']);
    $input['footertext'] = wp_filter_nohtml_kses($input['footertext']);

    $input['fburl'] = esc_url_raw($input['fburl']);
    $input['twitter'] = esc_url_raw($input['twitter']);
    $input['googleplus'] = esc_url_raw($input['googleplus']);
    $input['linkedin'] = esc_url_raw($input['linkedin']);

    return $input;
}

function vdh_framework_load_scripts() {
    wp_enqueue_media();
    wp_enqueue_style('vdh_framework', get_template_directory_uri() . '/theme-options/css/options.css', false, '1.0.0');
    wp_enqueue_style('vdh_framework');
    // Enqueue colorpicker scripts for versions below 3.5 for compatibility
    wp_enqueue_script('options-custom', get_template_directory_uri() . '/theme-options/js/options.js', array('jquery'));
    wp_enqueue_script('media-uploader', get_template_directory_uri() . '/theme-options/js/media-uploader.js', array('jquery'));
    wp_enqueue_script('media-uploader');
}

add_action('admin_enqueue_scripts', 'vdh_framework_load_scripts');

function vdh_framework_menu_settings() {
    $menu = array(
        'page_title' => __('VDH Theme Options', 'vdh'),
        'menu_title' => __('Theme Options', 'vdh'),
        'capability' => 'edit_theme_options',
        'menu_slug' => 'vdh_framework',
        'callback' => 'vdh_framework_page'
    );
    return apply_filters('vdh_framework_menu', $menu);
}

add_action('admin_menu', 'theme_options_add_page');

function theme_options_add_page() {
    $menu = vdh_framework_menu_settings();
    add_theme_page($menu['page_title'], $menu['menu_title'], $menu['capability'], $menu['menu_slug'], $menu['callback']);
}

function vdh_framework_page() {
    global $select_options;
    if (!isset($_REQUEST['settings-updated']))
        $_REQUEST['settings-updated'] = false;
    ?>
    <div class="fasterthemes-themes">
        <form method="post" action="options.php" id="form-option" class="theme_option_ft">
            <div class="fasterthemes-footer">
                <h1><?php _e('Theme Options', 'vdh'); ?></h1>
                <ul>
                    <li class="btn-save"><input type="submit" class="button-primary" value="<?php _e('Save options', 'vdh') ?>" /></li>
                </ul>
            </div>
            <div class="fasterthemes-details">
                <div class="fasterthemes-options">
                    <div class="right-box">
                        <div class="nav-tab-wrapper">
                            <ul>
                                <li><a id="options-group-1-tab" class="nav-tab basicsettings-tab" title="Basic Settings" href="#options-group-1"><?php _e('Basic Settings', 'vdh'); ?></a></li>
                                <li><a id="options-group-2-tab" class="nav-tab socialsettings-tab" title="Social Settings" href="#options-group-2"><?php _e('Social Settings', 'vdh'); ?></a></li>
                                <li><a id="options-group-3-tab" class="nav-tab ckansettings-tab" title="CKAN Settings" href="#options-group-3"><?php _e('CKAN Settings', 'vdh'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="right-box-bg"></div>
                    <div class="postbox left-box"> 
                        <!--======================== F I N A L - - T H E M E - - O P T I O N ===================-->
                        <?php
                        settings_fields('vdh_options');
                        $vdh_options = get_option('vdh_theme_options');
                        ?>

                        <!-------------- First group ----------------->
                        <div id="options-group-1" class="group faster-inner-tabs">
                            <div class="section theme-tabs theme-logo">
                                <a class="heading faster-inner-tab active" href="javascript:void(0)"><?php _e('Site Logo', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group active">
                                    <div class="explain"><?php _e('Size of logo should be exactly 117x43px for best results. Leave blank to use text heading.', 'vdh') ?></div>
                                    <div class="ft-control">
                                        <span style="float:left;font-size:14px;margin:14px 4px;"><?php echo bloginfo('url') ?></span>
                                        <input id="logo-img" class="upload" type="text" name="vdh_theme_options[logo]" value="<?php
                                        if (!empty($vdh_options['logo'])) {
                                            echo esc_url($vdh_options['logo']);
                                        }
                                        ?>" placeholder="<?php _e('No file chosen', 'vdh') ?>" />
                                        <button id="default_logo" class="button">Restore default</button>
                                        <script type="text/javascript">
                                            document.getElementById('default_logo').onclick = function(e) {
                                                e.preventDefault();
                                                document.getElementById('logo-img').value = '/img/vdh-logo.png';
                                            };
                                        </script>
                                        <div class="screenshot" id="logo-image">
                                            <?php
                                            if (!empty($vdh_options['logo'])) {
                                                echo "<img src='" . esc_url($vdh_options['logo']) . "' /><a class='remove-image'>Remove</a>";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section theme-tabs theme-favicon">
                                <a class="heading faster-inner-tab" href="javascript:void(0)"><?php _e('Favicon', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group">
                                    <div class="explain"><?php _e('Size of favicon should be exactly 32x32px for best results.', 'vdh'); ?></div>
                                    <div class="ft-control">
                                        <input id="favicon-img" class="upload" type="text" name="vdh_theme_options[favicon]" 
                                               value="<?php
                                               if (!empty($vdh_options['favicon'])) {
                                                   echo esc_url($vdh_options['favicon']);
                                               }
                                               ?>" placeholder="<?php _e('No file chosen', 'vdh') ?>" />
                                        <input id="upload_image_button11" class="upload-button button" type="button" value="<?php _e('Upload', 'vdh') ?>" />
                                        <div class="screenshot" id="favicon-image">
                                            <?php
                                            if (!empty($vdh_options['favicon'])) {
                                                echo "<img src='" . esc_url($vdh_options['favicon']) . "' /><a class='remove-image'>Remove</a>";
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="slider-settings" class="section theme-tabs">
                                <a class="heading faster-inner-tab" href="javascript:void(0)"><?php _e('Slider Settings', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Choose the slider category', 'vdh'); ?></div>
                                        <select name="vdh_theme_options[frontend_slider]">
                                            <option value="">Choose the slider category</option>
                                            <?php $categories = get_categories(array('hide_empty' => false)); ?>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category->cat_ID ?>"
                                                <?php echo (isset($vdh_options['frontend_slider']) && $category->cat_ID == $vdh_options['frontend_slider'] ? 'selected="selected"' : '') ?>
                                                        ><?php echo $category->cat_name ?></option>
                                                    <?php endforeach; ?>
                                        </select>
                                    </div>                
                                </div>
                            </div>
                            <div id="section-footertext2" class="section theme-tabs">
                                <a class="heading faster-inner-tab" href="javascript:void(0)"><?php _e('Copyright Text', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Some text regarding copyright of your site, you would like to display in the footer.', 'vdh'); ?></div>                
                                        <input type="text" placeholder="<?php _e('Copyright Text', 'vdh'); ?>" id="footertext2" class="of-input" name="vdh_theme_options[footertext]" size="32"  value="<?php
                                        if (!empty($vdh_options['footertext'])) {
                                            echo esc_attr($vdh_options['footertext']);
                                        }
                                        ?>">
                                    </div>                
                                </div>
                            </div> 
                        </div>             
                        <!-------------- Second group ----------------->
                        <div id="options-group-2" class="group faster-inner-tabs">            
                            <div id="section-facebook" class="section theme-tabs">
                                <a class="heading faster-inner-tab active" href="javascript:void(0)"><?php _e('Facebook', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group active">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Facebook profile or page URL i.e.', 'vdh'); ?> http://facebook.com/username/ </div>                
                                        <input id="facebook" class="of-input" name="vdh_theme_options[fburl]" size="30" type="text" value="<?php
                                        if (!empty($vdh_options['fburl'])) {
                                            echo esc_url($vdh_options['fburl']);
                                        }
                                        ?>" />
                                    </div>                
                                </div>
                            </div>
                            <div id="section-twitter" class="section theme-tabs">
                                <a class="heading faster-inner-tab" href="javascript:void(0)"><?php _e('Twitter', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Twitter profile or page URL i.e.', 'vdh'); ?> http://www.twitter.com/username/</div>                
                                        <input id="twitter" class="of-input" name="vdh_theme_options[twitter]" type="text" size="30" value="<?php
                                        if (!empty($vdh_options['twitter'])) {
                                            echo esc_url($vdh_options['twitter']);
                                        }
                                        ?>" />
                                    </div>                
                                </div>
                            </div>
                            <div id="section-googleplus" class="section theme-tabs">
                                <a class="heading faster-inner-tab" href="javascript:void(0)"><?php _e('Google Plus', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Google Plus profile or page URL i.e.', 'vdh'); ?> https://plus.google.com/username/</div>                
                                        <input id="googleplus" class="of-input" name="vdh_theme_options[googleplus]" type="text" size="30" value="<?php
                                        if (!empty($vdh_options['googleplus'])) {
                                            echo esc_url($vdh_options['googleplus']);
                                        }
                                        ?>" />
                                    </div>                
                                </div>
                            </div>

                            <div id="section-linkedin" class="section theme-tabs">
                                <a class="heading faster-inner-tab" href="javascript:void(0)"><?php _e('Linkedin', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Linkedin profile or page URL i.e.', 'vdh'); ?> https://www.linkedin.com/username/</div>                
                                        <input id="pintrest" class="of-input" name="vdh_theme_options[linkedin]" type="text" size="30" value="<?php
                                        if (!empty($vdh_options['linkedin'])) {
                                            echo esc_url($vdh_options['linkedin']);
                                        }
                                        ?>" />
                                    </div>                
                                </div>
                            </div>
                        </div>   
                        <!-------------- Third group ----------------->
                        <div id="options-group-3" class="group faster-inner-tabs">            
                            <div id="section-facebook" class="section theme-tabs">
                                <a class="heading faster-inner-tab active" href="javascript:void(0)"><?php _e('CKAN Url', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group active">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Please Enter URL of your CKAN instance', 'vdh'); ?> </div>                
                                        <input id="ckan" class="of-input" name="vdh_theme_options[ckan_url]" size="30" type="text" value="<?php
                                        if (!empty($vdh_options['ckan_url'])) {
                                            echo esc_url($vdh_options['ckan_url']);
                                        }
                                        ?>" />
                                    </div>                
                                </div>
                            </div>
                            <div id="section-facebook" class="section theme-tabs">
                                <a class="heading faster-inner-tab" href="javascript:void(0)"><?php _e('Recompile Static Files', 'vdh'); ?></a>
                                <div class="faster-inner-tab-group">
                                    <div class="ft-control">
                                        <div class="explain"><?php _e('Make sure you know what you\'re doing', 'vdh'); ?> </div>                
                                        <button id="recompile_css" class="button">Recompile minified css</button>
                                        <button id="recompile_js" class="button">Recompile minified js</button>
                                        <script type="text/javascript">
                                            function recompile_static(e) {
                                                var url = e.target.id.replace('_', '/');
                                                e.preventDefault();
                                                if (confirm("We hope you know what you're doing. Do you really want to recompile the static " + url.split('/')[1] + " file?")) {
                                                    jQuery.get('/api/header/' + url, function(d) {
                                                        alert(d.msg || d.error);
                                                    });
                                                }
                                            }
                                            document.getElementById('recompile_css').onclick = recompile_static;
                                            document.getElementById('recompile_js').onclick = recompile_static;
                                        </script>
                                    </div>                
                                </div>
                            </div>
                        </div>   
                        <!--======================== F I N A L - - T H E M E - - O P T I O N S ===================--> 
                    </div>
                </div>
            </div>
            <div class="fasterthemes-footer">
                <ul>
                    <li class="btn-save"><input type="submit" class="button-primary" value="<?php _e('Save options', 'vdh') ?>" /></li>
                </ul>
            </div>
        </form>    
    </div>
    <div class="save-options"><h2><?php _e('Options saved successfully.', 'vdh'); ?></h2></div>
<?php } ?>
