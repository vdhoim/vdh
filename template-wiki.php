<?php
/*
Template name: Wiki template
*/ 
get_header();
?>
<div id="content" class="clearfix">
  <div id="main" class="col-sm-8 clearfix nopadding-left" role="main">
    <div id="home-main" class="home-main home">
      <header>
        <div class="page-catheader">
          <h2 class="page-title"><?php _e('Article Categories','vdh'); ?></h2>          
        </div>
      </header>
        <?php
	$vdh_cat = array(
			'child_of'                 => 0,
			'parent'                   => '',
			'orderby'                  => 'name',
			'order'                    => 'ASC',
			'hide_empty'               => 0,
			'hierarchical'             => 1,
			'include'                  => '',
			'exclude'                  => '',
			'number'                   => '',
			'taxonomy'                 => 'category',
			'pad_counts'               => false
			 );
	 
	 $vdh_cat = get_categories( $vdh_cat ); 
		$vdh_i=0;
		foreach ($vdh_cat as $vdh_categories) {
			$vdh_i++;
			if($vdh_i<5)$vdh_cat_id="cat-id"; else $vdh_cat_id='';
			if($vdh_i % 2 == 1)
			{				
				echo"<div class='border-bottom' style='float:left;'>";
			}
			
			?>
        
        <div class="cat-main-section <?php echo $vdh_cat_id?>">
          <header>
           <a href="<?php echo get_category_link( $vdh_categories->term_id );?>"> <h4> <?php echo $vdh_categories->name ;?> <span>(<?php echo $vdh_categories->count?>)</span></h4> </a>
          </header>
          <?php
								$vdh_args = array(
'posts_per_page' => 5,
	'tax_query' => array(
		'relation' => 'AND',
		array(
		'taxonomy' => 'category',
			'field' => 'id',
			'terms' => array($vdh_categories->term_id),
			
		),
	)
); $vdh_cat_post = new WP_Query( $vdh_args );
if ( $vdh_cat_post->have_posts() ) :?>
          <div class="content-according">
          	<ul>
            <?php while ( $vdh_cat_post->have_posts() ):$vdh_cat_post->the_post(); ?>
            <li><a href="<?php echo get_permalink($post->ID)?>">
              <?php the_title();?>
            </a></li>
            <?php endwhile;?>
            </ul>
          </div>
          <?php endif;?>
        </div>
        <?php 	
		if($vdh_i % 2 ==0)
			{
				echo"</div>";
			}
		}
		if($vdh_i % 2 ==1)
			{
				echo"</div>";
			}
		?> 
    </div>    
    <!-- end #main --> 
  </div>  
  <?php get_sidebar(); // sidebar 1 ?>
</div></div>
<!-- end #content -->
<?php get_footer();?>
