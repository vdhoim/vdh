<?php $vdh_options = get_option('vdh_theme_options'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html <?php language_attributes(); ?> class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html <?php language_attributes(); ?> class="no-js"> <![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <?php if (!empty($vdh_options['favicon'])): ?>
        <link rel="shortcut icon" href="<?php echo esc_url($vdh_options['favicon']); ?>">
    <?php endif; ?>
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta name="description" content="">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="mainmenu-wrapper">
        <div class="container">
            <div class="menuextras">
                <div class="extras">
                    <?php
                    if (has_nav_menu('top')) {
                        wp_nav_menu(
                                array(
                                    'theme_location' => 'top'
                        ));
                    }
                    ?>
                </div>
            </div>
            <div class="navbar-collapse collapse top-menu">  
                <?php
                wp_nav_menu(
                        array(
                            'theme_location' => 'primary',
                            'container' => 'nav',
                            'container_class' => 'mainmenu',
                            'container_id' => '',
                            'menu_class' => 'mainmenu',
                            'echo' => true,
                            'fallback_cb' => 'wp_page_menu',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'items_wrap' => '<ul id="mainmenu">%3$s</ul>',
                            'depth' => 3,
                            'walker' => new New_Walker_Nav_Menu
                        )
                );
                ?>
            </div>
        </div>
    </div>