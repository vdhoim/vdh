<?php

class Vdh_Template_Functions {

    private $_vdh_options;
    private $dir;

    static function get_instance() {
        return new self;
    }

    public function init() {
        require get_template_directory() . '/function/custom-header.php';
        add_action('wp_enqueue_scripts', array($this, 'vdh_theme_setup'));
        add_action('after_setup_theme', array($this, 'vdh_setup'));
        add_filter('wp_nav_menu', array($this, 'vdh_add_logo'));
        register_nav_menus(
                array(
                    'primary' => __('The Main Menu', 'vdh'), // main nav in header
                    'top' => __('Top Header Menu', 'vdh'), // top menu in header
                    'footer-links' => __('Footer Links', 'vdh'), // footer_1
                )
        );
        wp_register_sidebar_widget(
                __('Category Widget', 'vdh'), // your unique widget id
                __('Category Widget', 'vdh'), // widget name
                array($this, 'vdh_category_widget_function'), // callback function
                array('description' => __('Category Widget Shows Category', 'vdh'))
        );
        add_action('widgets_init', array($this, 'vdh_popular_load_widgets'));
        register_sidebar(array(
            'id' => 'sidebar1',
            'name' => __('Main Sidebar', 'vdh'),
            'description' => __('Used on every page.', 'vdh'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widgettitle">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'id' => 'footer1',
            'name' => __('Footer Content Area 1', 'vdh'),
            'description' => __('Used on Footer.', 'vdh'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widgettitle">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'id' => 'footer2',
            'name' => __('Footer Content Area 2', 'vdh'),
            'description' => __('Used on Footer.', 'vdh'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widgettitle">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'id' => 'footer3',
            'name' => __('Footer Content Area 3', 'vdh'),
            'description' => __('Used on Footer.', 'vdh'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widgettitle">',
            'after_title' => '</h4>',
        ));
        if (function_exists('add_theme_support')) {
            add_theme_support('post-thumbnails');
            set_post_thumbnail_size(150, 150); // default Post Thumbnail dimensions   
        }
        if (function_exists('add_image_size')) {
            add_image_size('category-thumb', 300, 9999); //300 pixels wide (and unlimited height)
            add_image_size('homepage-thumb', 220, 180, true); //(cropped)
        }
        add_theme_support('html5', array('comment-list', 'comment-form', 'search-form'));
        add_filter('wp_title', array($this, 'vdh_wp_title'), 10, 2);
        add_action('wp_ajax_vdh_search', array($this, 'vdh_search'));
        add_action('wp_ajax_nopriv_vdh_search', array($this, 'vdh_search'));
        add_action('wp_ajax_vdh_header', array($this, 'vdh_header_image_function'));
        add_action('wp_ajax_nopriv_vdh_header', array($this, 'vdh_header_image_function'));
        add_filter('get_the_excerpt', array($this, 'vdh_trim_excerpt'));
        add_filter('wp_nav_menu_objects', array($this, 'bootstrapify_nav_menu'));
        add_filter('walker_nav_menu_start_el', array($this, 'bootstrapify_nav_dropdown'), 10, 4);
        add_action('pre_get_posts', array($this, 'exclude_frontend_slider_category'));
        add_action('parse_query', array($this, 'define_call_from_home_to_exclude_frontend_slider_category'));
        add_action('template_redirect', array($this, 'redirect_from_frontend_slider_post_url'));
    }

    public function exclude_frontend_slider_category($query) {
        if (!isset($query->is_front_slider) && !current_user_can('administrator')) {
            $query->set('cat', '-' . (string) $this->get_vdh_options('frontend_slider'));
        }
    }

    public function redirect_from_frontend_slider_post_url() {
        global $post;
        if (isset($post->ID) && is_single($post->ID) && in_category($this->get_vdh_options('frontend_slider'), $post)) {
            wp_redirect('/', 301);
            exit;
        }
    }

    public function define_call_from_home_to_exclude_frontend_slider_category($query) {
        if (isset($query->query['posts_per_page']) && $query->query['posts_per_page'] === 99987) {
            $query->is_front_slider = true;
        }
    }

    public function bootstrapify_nav_dropdown($item_output, $item, $depth, $args) {
        if ($item->menu_item_parent !== '0' && $item->type === 'custom' && $item->url === '#') {
            return $this->_maybe_build_header_link($item);
        }
        return $item_output;
    }

    private function _maybe_build_header_link($item) {
        $header = $item->title;
        if (in_array($header, array('Organizations', 'Datasets', 'Groups'))) {
            $header = '<a href="' . get_option('ckan_url') . strtolower(substr($header, 0, -1)) . '/" '
                    . 'title="' . __($header, "vdh") . '">' . __($header, "vdh") . "</a>";
        }
        return "<h4>$header</h4>\n";
    }

    public function bootstrapify_nav_menu($objects) {
        foreach ($this->_add_ckan_header($objects) as $object) {
            if (in_array('current-menu-item', $object->classes, true) || in_array('current_page_item', $object->classes, true) || in_array('current-menu-ancestor', $object->classes, true)) {
                $object->classes = array_diff($object->classes, array('current-menu-item', 'current-menu-ancestor', 'current_page_item', 'active'));
                $object->classes[] = 'active';
            }
            if (in_array('menu-item-has-children', $object->classes, true)) {
                $object->classes = array_diff($object->classes, array('menu-item-has-children', 'has-submenu'));
                $object->classes[] = 'has-submenu';
            }
        }
        return $objects;
    }

    private function _add_ckan_header(&$objects) {
        $o = null;
        foreach ($objects as $obj) {
            $options = $this->get_vdh_options();
            if ($obj->url === $options['ckan_url']) {
                $obj->classes[] = 'menu-item-has-children';
                $o = clone $obj;
                break;
            }
        }
        if ($o instanceof WP_Post) {

            function _add_children($arr, $obj, &$objects) {
                foreach ($arr as $el) {
                    $o = clone $obj;
                    $o->ID = $el['id'];
                    $o->object_id = $o->db_id = (string) $el['id'];
                    $o->url = $o->guid = $el['url'];
                    $o->title = $o->post_title = $el['name'];
                    $o->menu_item_parent = (string) $el['parent'];
                    $objects[] = $o;
                    if (isset($el['children']) && $el['children']) {
                        _add_children($el['children'], $o, $objects);
                    }
                }
            }

            $ckan_api = new Ckan_Api($o->url);
            $ckan_header = $ckan_api->get_header_menu($o->ID);
            _add_children($ckan_header, $o, $objects);
        }
        return $objects;
    }

    public function get_vdh_options($key = null) {
        if (!$this->_vdh_options) {
            $this->_vdh_options = get_option('vdh_theme_options');
        }
        if($key){
            return isset($this->_vdh_options[$key]) ? $this->_vdh_options[$key] : null; 
        }
        return $this->_vdh_options;
    }

    private function get_dir() {
        if (!$this->dir) {
            $this->dir = dirname(__FILE__);
        }
        return $this->dir;
    }

    private function add_static_file($type) {
        $dir = $this->get_dir();
        $file = "/$type/main.min.$type";
        if (!file_exists($dir . $file) || (isset($_REQUEST['recompile']) && $_REQUEST['recompile'] === $type)) {
            ini_set('memory_limit', '256M');
            $output = null;
            switch ($type) {
                case 'css':
                    require_once $dir . '/lib/less/Less.php';
                    $parser = new Less_Parser(array('compress' => true));
                    $parser->parseFile($dir . '/less/main.less');
                    $output = preg_replace('/(?<=\;)[^;]*?\(jseval\)\;?/', '', $parser->getCss());
                    break;
                case 'js':
                    require_once $dir . '/lib/jsMinifier/Minifier.php';
                    $scripts = array(
                        '/js/jquery-1.9.1.min.js',
                        '/js/bootstrap.min.js',
                        '/js/jquery.sequence-min.js',
                        '/js/jquery.bxslider.js',
                        '/js/main-menu.js',
                        '/js/template.js');
                    foreach ($scripts as $script) {
                        $js = file_get_contents($dir . $script);
                        $output .= Minifier::minify($js, array('flaggedComments' => false));
                    }
                    break;
                default:
                    break;
            }
            if ($output) {
                file_put_contents($dir . $file, $output);
            }
        }
        return $file;
    }

    public function vdh_theme_setup() {

        wp_enqueue_style('main.min.css', $this->add_static_file('css'));

        wp_enqueue_script('modernizr', '/js/modernizr-2.6.2-respond-1.1.0.min.js');

        wp_enqueue_script('main.min.js', $this->add_static_file('js'), array(), '0.1', true);

        wp_enqueue_script('general', get_template_directory_uri() . '/js/general.js', array(), '0.1', true);
        wp_localize_script('general', 'my_ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
        if (is_singular()) {
            wp_enqueue_script('comment-reply');
        }
    }

    public function vdh_setup() {
        /* content width */
        global $content_width;
        if (!isset($content_width)) {
            $content_width = 900;
        }
        /*
         * Make vdh theme available for translation.
         */
        load_theme_textdomain('vdh', get_template_directory() . '/languages');
        // This theme styles the visual editor to resemble the theme style.
        add_editor_style('css/editor-style.css');
        // Add RSS feed links to <head> for posts and comments.
        add_theme_support('automatic-feed-links');
        /*
         * Enable support for Post Formats.
         */
        // This theme allows users to set a custom background.
        add_theme_support('custom-background', apply_filters('vdh_custom_background_args', array('default-color' => '048eb0')));
        // Add support for featured content.
        add_theme_support('featured-content', array(
            'featured_content_filter' => 'vdh_get_featured_posts',
            'max_posts' => 6,
        ));
        // This theme uses its own gallery styles.
        add_filter('use_default_gallery_style', '__return_false');
    }

    public function vdh_add_logo($html) {

        $vdh_options = $this->get_vdh_options();

        $logo = '<li class="logo-wrapper">';
        $logo .= '<a href="' . get_bloginfo('url', 'display') . '">';
        $logo .= '<img src="' . $vdh_options['logo'] . '" alt="' . get_bloginfo('name', 'display') . '">';
        $logo .= '</a></li>';
        return preg_replace('/<ul\s+id=\"mainmenu\">/', '<ul id="mainmenu">' . $logo, $html);
    }

    public function vdh_popular_load_widgets() {
        register_widget('vdh_popular_widget');
        register_widget('vdh_recentpost_widget');
    }

    public function vdh_wp_title($title, $sep) {
        global $paged, $page;
        if (is_feed()) {
            return $title;
        }
        // Add the site name.
        $title .= get_bloginfo('name');
        // Add the site description for the home/front page.
        $vdh_site_description = get_bloginfo('description', 'display');
        if ($vdh_site_description && ( is_home() || is_front_page() )) {
            $title = "$title $sep $vdh_site_description";
        }
        // Add a page number if necessary.
        if ($paged >= 2 || $page >= 2) {
            $title = "$title $sep " . sprintf(__('Page %s', 'vdh'), max($paged, $page));
        }
        return $title;
    }

    public function vdh_search() {
        global $wpdb;
        $vdh_title = trim($_POST['queryString']);
        $vdh_args = array('posts_per_page' => -1, 'order' => 'ASC', "orderby" => "title", "post_type" => "post", 'post_status' => 'publish', "s" => $vdh_title);
        $vdh_posts = get_posts($vdh_args);
        $vdh_output = '';
        if ($vdh_posts) {
            $vdh_h = 0;
            $vdh_output.='<ul id="search-result">';
            foreach ($vdh_posts as $vdh_post) {
                $vdh_output.='<li class="que-icn">';
                $vdh_output.='<a href="' . $vdh_posts[$vdh_h]->guid . '">' . $vdh_posts[$vdh_h]->post_title . '</a>';
                $vdh_output.='</li>';
                $vdh_h++;
            }
            $vdh_output.='</ul>';
            echo $vdh_output;
        } else {
            $vdh_output.='no';
            echo $vdh_output;
        }
        die();
    }

    public function vdh_header_image_function() {
        $vdh_return['header'] = get_header_image();
        echo json_encode($vdh_return);
        die;
    }

    public function vdh_trim_excerpt($vdh_text) {
        $text = substr($vdh_text, 0, -10);
        return $text . '..<div class="clear-fix"></div><a href="' . get_permalink() . '" title="' . __('read more...', 'vdh') . '">' . __('Read more', 'vdh') . '</a>';
    }

    public function vdh_category_widget_function($vdh_args) {
        extract($vdh_args);
        echo $before_widget;
        echo $before_title . '<p class="wid-category"><span>' . __('Categories', 'vdh') . '</span></p>' . $after_title;

        echo $after_widget;
        // print some HTML for the widget to display here
        $vdh_cat = array(
            'child_of' => 0,
            'parent' => '',
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => 0,
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'number' => '',
            'taxonomy' => 'category',
            'pad_counts' => false
        );

        $vdh_cat = get_categories($vdh_cat);
        echo "<div class='wid-cat-container'><ul>";
        foreach ($vdh_cat as $vdh_categories) {
            ?>
            <li><a href="<?php echo get_category_link($vdh_categories->term_id); ?>" class="wid-cat-title"><?php echo $vdh_categories->name; ?>
                </a></li>
            <?php
        }
        echo "</ul></div>";
    }

    public static function add_template($template, array $vars = array()) {
        extract($vars);
        include $template . '.php';
    }

    public static function title_html($title) {
        $t = explode('[', $title);
        $style = self::_get_style($title);
        return "<h1$style\">$t[0]</h1>";
    }

    private static function _get_style($line) {
        $style = ' style="position:relative;';
        $s = explode('[', $line);
        if (isset($s[1]) && count($p = explode(':', $s[1])) > 1) {
            if ($p[0] === '') {
                $style .= 'text-align:left;padding-left:' . (int) $p[1] . '%;';
            } elseif (rtrim($p[1], ']') === '') {
                $style .= 'text-align:right;padding-right:' . (int) $p[0] . '%;';
            } else {
                $style = ' style="' .
                        ($p[0] >= 0 ? 'left:' : 'right:') .
                        abs($p[0]) . '%;' .
                        ($p[1] >= 0 ? 'top:' : 'bottom:') .
                        abs($p[1]) . '%;';
            }
        }
        return $style . '"';
    }

    public static function content_html($content) {
        $html = '';
        $c = explode('button|', $content);
        $ca = array_map('trim', explode("\n", $c[0]));
        foreach ($ca as $line) {
            $s = explode('[', $line);
            $style = self::_get_style($line);
            if ($s[0]) {
                $html .= "<p$style>$s[0]</p>";
            }
        }
        if (isset($c[1]) && count($b = explode('|', $c[1])) > 1) {
            $button_text = explode('[', $b[1]);
            $button_style = self::_get_style($b[1]);
            $html .= '<p' . $button_style . '><a class="btn btn-lg btn-primary" href="'
                    . ($b[0] ? $b[0] : '#') . '" target="_blank">'
                    . ($button_text[0] ? $button_text[0] : 'Learn More') . '</a></p>';
        }
        return $html;
    }

    static function vdh_entry_meta() {
        $vdh_category_list = get_the_category_list(', ');
        $vdh_tag_list = get_the_tag_list('', ', ');
        $vdh_date = sprintf('<a href="%1$s" title="%2$s" ><time datetime="%3$s">%4$s</time></a>', esc_url(get_permalink()), esc_attr(get_the_time()), esc_attr(get_the_date('c')), esc_html(get_the_date())
        );
        $vdh_author = sprintf('<span><a href="%1$s" title="%2$s" >%3$s</a></span>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), esc_attr(sprintf(__('View all posts by %s', 'vdh'), get_the_author())), get_the_author()
        );
        if ($vdh_tag_list) {
            $vdh_utility_text = __('Posted %3$s by %4$s & filed under %1$s Comments: ' . get_comments_number() . '.', 'vdh');
        } elseif ($vdh_category_list) {
            $vdh_utility_text = __('Posted %3$s by %4$s & filed under %1$s Comments: ' . get_comments_number() . '.', 'vdh');
        } else {
            $vdh_utility_text = __('Posted %3$s by %4$s Comments:' . get_comments_number() . '.', 'vdh');
        }
        printf(
                $vdh_utility_text, $vdh_category_list, $vdh_tag_list, $vdh_date, $vdh_author
        );
    }

}

class New_Walker_Nav_Menu extends Walker_Nav_Menu {

    private function adjust_start_menu_item($item, $id, $class_names) {
        if ($item->menu_item_parent !== '0' && $item->type === 'custom' && $item->url === '#') {
            return "<div>\n";
        }
        return '<li' . $id . $class_names . '>';
    }

    private function adjust_end_menu_item($item) {
        if ($item->menu_item_parent !== '0' && $item->type === 'custom' && $item->url === '#') {
            return "</div>\n";
        }
        return "</li>\n";
    }

    public function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        if ($depth == 0) {
            $output .= "\n$indent<div class=\"mainmenu-submenu\">";
            $output .= "\n$indent\t<div class=\"mainmenu-submenu-inner\">";
        } else {
            $output .= "\n$indent<ul class=\"sub-menu\">\n";
        }
    }

    public function end_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        if ($depth == 0) {
            $output .= "\n$indent\t</div>";
            $output .= "\n$indent</div>";
        } else {
            $output .= "$indent</ul>\n";
        }
    }

    /**
     * Start the element output.
     *
     * @see Walker::start_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        $indent = ( $depth ) ? str_repeat("\t", $depth) : '';

        $classes = empty($item->classes) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        /**
         * Filter the CSS class(es) applied to a menu item's <li>.
         *
         * @since 3.0.0
         *
         * @see wp_nav_menu()
         *
         * @param array  $classes The CSS classes that are applied to the menu item's <li>.
         * @param object $item    The current menu item.
         * @param array  $args    An array of wp_nav_menu() arguments.
         */
        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        /**
         * Filter the ID applied to a menu item's <li>.
         *
         * @since 3.0.1
         *
         * @see wp_nav_menu()
         *
         * @param string $menu_id The ID that is applied to the menu item's <li>.
         * @param object $item    The current menu item.
         * @param array  $args    An array of wp_nav_menu() arguments.
         */
        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . $this->adjust_start_menu_item($item, $id, $class_names);

        $atts = array();
        $atts['title'] = !empty($item->attr_title) ? $item->attr_title : '';
        $atts['target'] = !empty($item->target) ? $item->target : '';
        $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';
        $atts['href'] = !empty($item->url) ? $item->url : '';

        /**
         * Filter the HTML attributes applied to a menu item's <a>.
         *
         * @since 3.6.0
         *
         * @see wp_nav_menu()
         *
         * @param array $atts {
         *     The HTML attributes applied to the menu item's <a>, empty strings are ignored.
         *
         *     @type string $title  Title attribute.
         *     @type string $target Target attribute.
         *     @type string $rel    The rel attribute.
         *     @type string $href   The href attribute.
         * }
         * @param object $item The current menu item.
         * @param array  $args An array of wp_nav_menu() arguments.
         */
        $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args);

        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ( 'href' === $attr ) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }
        $item_output = is_array($args) ? '' : $args->before;
        $item_output .= '<a' . $attributes . '>';
        /** This filter is documented in wp-includes/post-template.php */
        $item_output .= is_array($args) ? '' : $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= is_array($args) ? '' : $args->after;


        /**
         * Filter a menu item's starting output.
         *
         * The menu item's starting output only includes $args->before, the opening <a>,
         * the menu item's title, the closing </a>, and $args->after. Currently, there is
         * no filter for modifying the opening and closing <li> for a menu item.
         *
         * @since 3.0.0
         *
         * @see wp_nav_menu()
         *
         * @param string $item_output The menu item's starting HTML output.
         * @param object $item        Menu item data object.
         * @param int    $depth       Depth of menu item. Used for padding.
         * @param array  $args        An array of wp_nav_menu() arguments.
         */
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    /**
     * Ends the element output, if needed.
     *
     * @see Walker::end_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Page data object. Not used.
     * @param int    $depth  Depth of page. Not Used.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function end_el(&$output, $item, $depth = 0, $args = array()) {
        $output .= $this->adjust_end_menu_item($item);
    }

}

/** Define the Widget as an extension of WP_Widget * */
class vdh_popular_widget extends WP_Widget {

    function vdh_popular_widget() {
        /* Widget settings. */
        $vdh_widget_ops = array('classname' => 'widget_popular', 'description' => __('Displays most popular posts by comment count', 'vdh'));

        /* Widget control settings. */
        $vdh_control_ops = array('id_base' => 'popular-widget');

        /* Create the widget. */
        $this->WP_Widget('popular-widget', __('Popular Posts', 'vdh'), $vdh_widget_ops, $vdh_control_ops);
    }

// Limit to last 30 days
    function filter_where($where = '') {
// posts in the last 30 days
        $where .= " AND post_date > '" . date('Y-m-d', strtotime('-' . $instance['days'] . ' days')) . "'";
        return $where;
    }

    function widget($args, $instance) {
        extract($args);
        echo $before_widget;
        if (!empty($instance['title']))
            echo $before_title . '<p class="wid-category"><span>' . $instance['title'] . '</span></p>' . $after_title;
        $loop_args = array(
            'posts_per_page' => (int) $instance['count'],
            'orderby' => 'comment_count'
        );
        if (0 == $instance['days']) {
            $loop = new WP_Query($loop_args);
        } else {
            add_filter('posts_where', array($this, 'filter_where'));
            $loop = new WP_Query($loop_args);
            remove_filter('posts_where', array($this, 'filter_where'));
        }echo "<div class='wid-cat-container'><ul>";
        if ($loop->have_posts()): while ($loop->have_posts()): $loop->the_post();
                global $post;
                ?><li>
                    <a href="<?php echo get_permalink(); ?>" class="wid-cat-title wid-popular-post">
                        <?php the_title(); ?>
                    </a></li>
                <?php
            endwhile;
        endif;
        wp_reset_query();
        echo "</ul></div>";
        echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;

        /* Strip tags (if needed) and update the widget settings. */
        $vdh_instance['title'] = esc_attr($new_instance['title']);
        $vdh_instance['count'] = (int) $new_instance['count'];
        $vdh_instance['days'] = (int) $new_instance['days'];
        return $instance;
    }

    function form($instance) {
        /* Set up some default widget settings. */
        $vdh_defaults = array('title' => '', 'count' => 5, 'days' => 30);
        $instance = wp_parse_args((array) $instance, $vdh_defaults);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'vdh') ?>:</label>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Number of Posts', 'vdh') ?>:</label>
            <input id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" size="3" value="<?php echo $instance['count']; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('days'); ?>"><?php _e('Posted in the past X days', 'vdh') ?>:</label>
            <input id="<?php echo $this->get_field_id('days'); ?>" name="<?php echo $this->get_field_name('days'); ?>" size="3" value="<?php echo $instance['days']; ?>" />
        </p>
        <p class="description"><?php _e('Use 0 for no time limit.', 'vdh') ?></p>
        <?php
    }

}

class vdh_recentpost_widget extends WP_Widget {

    function vdh_recentpost_widget() {
        /* Widget settings. */
        $vdh_widget_ops = array('classname' => 'widget_recentpost', 'description' => __('Displays most recent posts by post count', 'vdh'));

        /* Widget control settings. */
        $vdh_control_ops = array('id_base' => 'recent-widget');

        /* Create the widget. */
        $this->WP_Widget('recent-widget', __('Recent Posts', 'vdh'), $vdh_widget_ops, $vdh_control_ops);
    }

    function widget($args, $instance) {
        extract($args);
        echo $before_widget;
        if (!empty($instance['title']))
            echo $before_title . '<p class="wid-category"><span>' . $instance['title'] . '</span></p>' . $after_title;
        $vdh_loop_args = array(
            'posts_per_page' => (int) $instance['count'],
            'orderby' => 'DESC'
        );
        $vdh_loop = new WP_Query($vdh_loop_args);
        echo "<div class='wid-cat-container'><ul>";
        if ($vdh_loop->have_posts()): while ($vdh_loop->have_posts()): $vdh_loop->the_post();
                global $post;
                ?><li>
                    <a href="<?php echo get_permalink(); ?>" class="wid-cat-title wid-popular-post"><?php the_title(); ?></a></li>
                    <?php
                endwhile;
            endif;
            wp_reset_query();
            echo "</ul></div>";
            echo $after_widget;
        }

        function update($new_instance, $old_instance) {
            $vdh_instance = $old_instance;

            /* Strip tags (if needed) and update the widget settings. */
            $vdh_instance['title'] = esc_attr($new_instance['title']);
            $vdh_instance['count'] = (int) $new_instance['count'];
            return $vdh_instance;
        }

        function form($instance) {
            /* Set up some default widget settings. */
            $vdh_defaults = array('title' => '', 'count' => 5, 'days' => 30);
            $instance = wp_parse_args((array) $instance, $vdh_defaults);
            ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'vdh') ?>:</label>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Number of Posts', 'vdh') ?>:</label>
            <input id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" size="3" value="<?php echo $instance['count']; ?>" />
        </p>
        <?php
    }

}

Vdh_Template_Functions::get_instance()->init();

require_once('theme-options/options.php');
