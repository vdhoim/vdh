</div>
<footer role="contentinfo" id="footer"> 
    <div class="footer">
        <div class="container">
            <?php $vdh_options = get_option('vdh_theme_options'); ?>
            <div id="widget-footer" class="clearfix row">
                <div class="col-footer col-md-4">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer1')) : ?>
                    <?php endif; ?>
                </div>
                <div class="col-footer col-md-4">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2')) : ?>
                    <?php endif; ?>
                </div>
                <div class="col-footer col-md-4">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer3')) : ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (!empty($vdh_options['footertext'])): ?>
                <nav class="footer-menu-nav">
                    <p class="attribution">
                        <?= esc_attr($vdh_options['footertext']); ?>
                    </p>
                </nav>
            <?php endif; ?>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>