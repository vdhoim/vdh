<?php Vdh_Template_Functions::add_template('slider/embedded_css') ?>
<?php if (count($slides) > 0): ?>
    <div id="slider">
        <div id="myCarousel" class="carousel slide">
            <?php if (count($slides) > 1): ?>
                <ol class="carousel-indicators">
                    <?php foreach ($slides as $key => $slide): ?>
                        <li data-target="#myCarousel" 
                            data-slide-to="<?php echo $key ?>" 
                            class="<?php echo (!$key ? 'active' : '') ?>"></li>
                        <?php endforeach; ?>
                </ol>
            <?php endif; ?>
            <div class="carousel-inner">
                <?php
                foreach ($slides as $key => $slide):
                    $image = '/img/blue_network.png';
                    if (has_post_thumbnail($slide->ID)) {
                        $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'single-post-thumbnail');
                        $image = $thumbnail[0];
                    }
                    ?>
                    <div class="item<?php echo (!$key ? ' active' : '') ?>">
                        <img src="<?php echo $image ?>" class="img-responsive" />
                        <div class="container">
                            <div class="carousel-caption">
                                <?php echo Vdh_Template_Functions::title_html($slide->post_title) ?>
                                <?php echo Vdh_Template_Functions::content_html($slide->post_content) ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php if (count($slides) > 1): ?>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="icon-prev"></span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="icon-next"></span>
                </a>
            <?php endif; ?>
        </div>

    </div>
<?php endif; ?>